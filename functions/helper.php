<?php

# Help functions for the theme

// Format Phone Numbers
function format_phone($phone, $format='hyphen') {

	// This function will format north american phone numbers. 
	// 3 formats available. hyphen is set as default.

		// hypen    = 604-555-1234
		// Brackets = (604) 555-1234
		// dots     = 604.555.1234

	$phone = preg_replace("/[^0-9]/", "", $phone);
	$length = strlen($phone);
	$format = strtolower($format);

	// Default phone with brackets
	if ($format == 'brackets') {

		switch($length){
			
			case 7:
				return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
			break;
			
			case 10:
				return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
			break;

			case 11:
				return preg_replace("/([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{4})/", "$1 ($2) $3-$4", $phone);
			break;

			default:
				return $phone;
			break;
		}

	}

	// Default phone with hyphen
	if ($format == 'hyphen') {

		switch($length){
			
			case 7:
				return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
			break;
			
			case 10:
				return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "$1-$2-$3", $phone);
			break;

			case 11:
				return preg_replace("/([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{4})/", "$1-$2-$3-$4", $phone);
			break;

			default:
				return $phone;
			break;
		}

	}


	// Default phone with dots
	if ($format == 'dots') {

		switch($length){
			
			case 7:
				return preg_replace("/([0-9]{3})([0-9]{4})/", "$1.$2", $phone);
			break;
			
			case 10:
				return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "$1.$2.$3", $phone);
			break;

			case 11:
				return preg_replace("/([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{4})/", "$1.$2.$3.$4", $phone);
			break;

			default:
				return $phone;
			break;
		}

	}
}
	
// Schema - Micro Data for google

	// Remove Yoast Schema JSON first
	function bybe_remove_yoast_json($data){
	$data = array();
		return $data;
	}
	add_filter('wpseo_json_ld_output', 'bybe_remove_yoast_json', 10, 1);

	// Client Schema
	add_action('wp_head', function() {

		$custom_logo_id = get_theme_mod( 'custom_logo' );
		$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );

		$schema = array(
			'@context'        => "http://schema.org",
			'@type'           => 'LocalBusiness',
			'name'            => get_bloginfo('name'),
			'url'             => get_home_url(),
			'address'         => array(
			'@type'           => 'PostalAddress',
			'streetAddress'   => do_shortcode('[lg-address1]'),
			'postalCode'      => do_shortcode('[lg-postcode]'),
			'addressLocality' => do_shortcode('[lg-city]'),
			'addressRegion'   => do_shortcode('[lg-province]'), ),
			'logo'            => $image[0],
			'image'           => $image[0],
			'telephone'       => '+1' . do_shortcode('[lg-phone-main]'),
		);

		echo '<!-- / WOW Schema. -->';
		echo '<script type="application/ld+json">' . json_encode($schema) . '</script>';

	});		

	

function short_string($string, $limit){
	$chars = preg_replace( "/\r|\n/", " ", $string );
	$chars = explode(' ',strip_tags($chars));
	$chars = array_slice($chars, 0, $limit);

	return join(" ", $chars);
}

function site_logo(){
	return has_custom_logo() ? get_custom_logo() : do_shortcode('[lg-site-logo]');
}

function is_blog () {
    return ( is_archive() || is_author() || is_category() || is_home() || is_single() || is_tag()) && 'post' == get_post_type();
}

?>