<?php
/**
 * Main template file
 *
 */
?>

<?php get_header(); ?>

	<div id="primary" class="blog">
		<main id="site-content" role="main">
			<div class="container">
				<div class="body-copy py-4">
					<div class="blog-banner mb-3">
						<?php if(has_post_thumbnail()): ?>
							<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
						<?php else: ?>
							<?php
								$banner = get_field('blog_default_banner', 'option');
							?>
							<img src="<?php echo $banner['url']; ?>" alt="<?php echo $banner['alt']; ?>">
						<?php endif; ?>
					</div>

					<h1 class="h2 mb-1"><?php the_title(); ?></h1>

					<div class="author mb-2 font-italic">Posted by <?php echo get_author_name(); ?></div>

					<?php the_content(); ?>
				</div>
				<?php get_sidebar(); ?>
			</div>
		</main>
	</div>

<?php get_footer(); ?>