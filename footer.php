<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	<!--<footer id="site-footer">
		
		<div id="site-footer-main" class="clearfix py-3 container-fluid justify-content-center align-items-start flex-wrap">
			<div class="row">
				<div class="site-footer-alpha col-md-4 text-center text-md-left"><?php dynamic_sidebar('footer-alpha'); ?></div>
				<div class="site-footer-bravo col-md-4 text-center text-md-left"><?php dynamic_sidebar('footer-bravo'); ?></div>
				<div class="site-footer-charlie col-md-4 text-center text-md-left"><?php dynamic_sidebar('footer-charlie'); ?></div>
			</div>
		</div>

		<div id="site-legal" class="py-3 px-3 d-flex justify-content-center justify-content-md-between align-items-center flex-wrap">
			<div class="site-info"><?php get_template_part("/templates/template-parts/footer/site-info"); ?></div>
			<div class="site-longevity"> <?php get_template_part("/templates/template-parts/footer/site-footer-longevity"); ?> </div>
		</div>

	</footer>-->

<?php wp_footer(); ?>

</body>
</html>
