<?php
	$side_image = get_field('side_image', 'option');
	$side_form = get_field('side_form', 'option');
	$side_video = get_field('side_video', 'option');
	$side_video_title = get_field('side_video_title', 'option');
	$side_footer_text = get_field('side_footer_text', 'option');
?>

	<img class="img-full" src="<?php echo $side_image['url']; ?>" alt="<?php echo $side_image['alt']; ?>">
	<div class="mt-1">
		Call Tom: <a href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo do_shortcode('[lg-phone-main]'); ?></a><br />and make this your home today
		<a class="d-block mt-2" href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><?php echo do_shortcode('[lg-email]'); ?></a>
	</div>