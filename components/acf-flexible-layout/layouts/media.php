<?php 
/**
 * Media Block Layout
 *
 */
?>

<?php

	get_template_part('/components/acf-flexible-layout/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

	$video = get_sub_field('youtube_video');
	$video_aspect_ratio = get_sub_field('video_aspect_ratio');
	$image = get_sub_field('image');
	$media_type = get_sub_field('media_type');
	$image_slider = get_sub_field('image_slider');
	$max_width = get_sub_field('max_width');

?>

<?php if($media_type == 'Video'): ?>
	<div class="embed-responsive embed-responsive-<?php echo $video_aspect_ratio; ?>" style="max-width: <?php echo $max_width; ?>; margin: auto auto;">
		<?php echo $video; ?>
	</div>
<?php endif; ?>

<?php if($media_type == 'Image'): ?>
	<div class="media-image" style="max-width: <?php echo $max_width; ?>; margin: auto auto;">
		<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="image-full">
	</div>
<?php endif; ?>

<?php if($media_type == 'Image Slider'): ?>
	<?php if($image_slider && is_array($image_slider)): ?>
		<div class="flexible-content-slider" style="max-width: <?php echo $max_width; ?>; margin: auto auto;">
		<?php foreach ($image_slider as $key => $slides): ?>
			<div>
				<img src="<?php echo $slides['slides']['url']; ?>" alt="<?php echo $slides['slides']['alt']; ?>">
			</div>
		<?php endforeach; ?>
		</div>

		<script>
			(function($) {

			    $(document).ready(function(){
			    	$('.flexible-content-slider').slick({
			    		slidesToShow: 1,
						slidesToScroll: 1,
						arrows: false,
						dots: true,
						fade: true,
			    		variableWidth: false,
			    		adaptiveHeight: true
					});
			    });

			}(jQuery));
		</script>
	<?php endif; ?>
<?php endif; ?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/components/acf-flexible-layout/partials/block-settings-end');

?>