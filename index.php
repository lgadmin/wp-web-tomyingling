<?php
/**
 * Main template file
 *
 */
?>

<?php get_header(); ?>

	<div id="primary">
		<main id="site-content" role="main">
			<div class="py-4 container">
				<div class="body-copy py-2">
					
					<div class="blog-banner mb-3">
						<?php
							$banner = get_field('blog_default_banner', 'option');
						?>
						<img src="<?php echo $banner['url']; ?>" alt="<?php echo $banner['alt']; ?>">
					</div>

					<?php get_template_part( 'templates/template-parts/content/content-loop'); ?>

				</div>
				<?php get_sidebar(); ?>
			</div>
		</main>
	</div>

<?php get_footer(); ?>