<?php
/**
 * The header for our theme
 *
 */
 
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">

	<header id="site-header">

    <!--<?php get_template_part("/templates/template-parts/header/utility-bar"); ?>-->

    <div class="header-main container">
      <div class="d-block d-md-flex justify-content-between align-items-center flex-wrap">
        <div class="site-branding px-3 py-2 py-md-1">
          <div class="logo">
            <?php echo site_logo(); ?>
          </div>
          <div class="mobile-toggle d-md-none"><i class="fa fa-bars" aria-hidden="true"></i></div>
        </div><!-- .site-branding -->

        <div class="d-block d-md-flex justify-content-end align-items-center pr-3">
          <div class="d-flex flex-column align-items-end">
            <?php get_template_part("/templates/template-parts/header/main-nav"); ?>
            <div class="d-none d-md-block"><?php echo do_shortcode('[lg-social-media]'); ?></div>
          </div>
          
          <img class="header-ballon d-none d-md-block" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/dist/images/Remax_Balloon.png">
        </div>
      </div>
    </div>

  </header><!-- #masthead -->
