<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<div id="primary">
		<div id="site-content" role="main">
			<main>

				<div class="row">
					<div class="side col-md-4">
						<?php get_template_part( '/templates/template-parts/content/content-side' ); ?>
					</div>

					<div class="main col-md-8">
						<?php get_template_part( '/templates/template-parts/content/content-top' ); ?>
						<?php get_template_part( '/components/acf-flexible-layout/layouts' ); ?>
					</div>
				</div>

			</main>
		</div>
	</div>

<?php get_footer(); ?>