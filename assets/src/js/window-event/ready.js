// Windows Ready Handler

(function($) {

    $(document).ready(function(){

      	//Mobile Toggle
        $('.mobile-toggle').on('click', function(){
      		$('.navbar-collapse').fadeToggle();
      	});

        $('.dropdown-toggle').on('mouseover', function(e){
          $(this).siblings('.dropdown-menu').addClass('show');
        });

        $('.dropdown-toggle').on('mouseleave', function(e){
          $(this).siblings('.dropdown-menu').removeClass('show');
        });

        //Mega Menu
        if($('.mega-menu-toggle')[0]){
          $('.mobile-toggle').on('click', function(){
            $('.mega-menu-toggle').click();
          });
        }
        
    });

}(jQuery));